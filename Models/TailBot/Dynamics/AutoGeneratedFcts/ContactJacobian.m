function J = ContactJacobian(x,y,phi,yF,phiH,phiT,g,lxC,lyC,lxB,lyB,lT,mS,mF,mT,jS,jF,jT,kF,kF0,bF)
%CONTACTJACOBIAN
%    J = CONTACTJACOBIAN(X,Y,PHI,YF,PHIH,PHIT,G,LXC,LYC,LXB,LYB,LT,MS,MF,MT,JS,JF,JT,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.4.
%    08-Jul-2014 11:40:37

t291 = phi+phiH;
t292 = cos(t291);
t293 = kF0+yF;
t294 = sin(t291);
t295 = cos(phi);
t296 = sin(phi);
t297 = t293.*t294;
J = reshape([1.0,0.0,0.0,1.0,-lxC.*t296+lyC.*t295-t292.*t293,t297-lxC.*t295-lyC.*t296,-t294,-t292,-t292.*t293,t297,0.0,0.0],[2, 6]);
