function SJ = StateJacobianWrapper(y_, p)

persistent contStateIndices
    if isempty(contStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
    end
    x      = y_(contStateIndices.x);
    y      = y_(contStateIndices.y);
    phi    = y_(contStateIndices.phi);
    yF     = y_(contStateIndices.yF);
    phiH   = y_(contStateIndices.phiH);
    phiT   = y_(contStateIndices.phiT);
    dx     = y_(contStateIndices.dx);
    dy     = y_(contStateIndices.dy);
    dphi   = y_(contStateIndices.dphi);
    dyF    = y_(contStateIndices.dyF);
    dphiH  = y_(contStateIndices.dphiH);
    dphiT  = y_(contStateIndices.dphiT);
    
    % Map the system parameters:
    % Keep the index-structs in memory to speed up processing
    persistent systParamIndices
    if isempty(systParamIndices)
        [~, ~, systParamIndices] = SystParamDefinition();
    end
    g = p(systParamIndices.g);
    lxC = p(systParamIndices.lxC);
    lyC = p(systParamIndices.lyC);
    lxB = p(systParamIndices.lxB);
    lyB = p(systParamIndices.lyB);
    lT = p(systParamIndices.lT);
    mS = p(systParamIndices.mS);
    mF = p(systParamIndices.mF);
    mT = p(systParamIndices.mT);
    jS = p(systParamIndices.jS);
    jF = p(systParamIndices.jF);
    jT = p(systParamIndices.jT);
    kF = p(systParamIndices.kF);
    kF0 = p(systParamIndices.kF0);
    bF = p(systParamIndices.bF);
    
    %%
%     x,y,phi,yF,phiH,phiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF
    SJ = StateJacobian(x,y,phi,yF,phiH,phiT, dx,dy,dphi,dyF,dphiH,dphiT, g, lxC,lyC,lxB,lyB,lT, mS,mF,mT,jS,jF,jT,kF,kF0,bF);

end