function [squigglyA, squigglyB, x0] = squiggleFunctionR(y_,p,deltaT,nMu,N)
        % squiggle function, adapted for use with remySimulator
        % Seems as if with Remy's reflection stuff, there's no need for the
        % dummy function?
        %% unwrap
        
        persistent contStateIndices
        if isempty(contStateIndices)
            [~, ~, contStateIndices] = ContStateDefinition();
        end
%         x      = y_(contStateIndices.x);
%         y     = y_(contStateIndices.y);
%         phi    = y_(contStateIndices.phi);
%         yF     = y_(contStateIndices.yF);
%         phiH   = y_(contStateIndices.phiH);
%         phiT   = y_(contStateIndices.phiT);
        
        
        
        %%
        nDoF = 6; % PUT THIS INTO PARAMETERS!
%         N = size(y_,2);
        allA = cell(1,N);
        allB = cell(1,N);
        for(i=1:N) % Can parallelize
%             deltaT = 0.01; % FIX THIS!
            [Ak,Bk] = getAB(y_(:,i), p,deltaT,nDoF);
            allA{i} = Ak;
            allB{i} = Bk;
        end
        
        squigglyA = cell(N+1,1); % +1 to include eye
        squigglyA{1} = eye(2*nDoF+1); % +1 to include imaginary dummy state z
        squigglyB = cell(N+1,N); % CHECK SIZE!
        
%         y0 = y_(:,1);
%         x0 = [ y_(contStateIndices.x);
%                y_(contStateIndices.y);
%                y_(contStateIndices.phi);
%                y_(contStateIndices.yF);
%                y_(contStateIndices.phiH);
%                y_(contStateIndices.phiT);
%                y_(contStateIndices.dx);
%                y_(contStateIndices.dy);
%                y_(contStateIndices.dphi);
%                y_(contStateIndices.dyF);
%                y_(contStateIndices.dphiH);
%                y_(contStateIndices.dphiT)
%                1];                          % dummy variable z
        
       x0 = getSystemStates(y_(:,1));
       x0 = [x0;1]; % adding dummy variable z
       
        
        
        % Parallelize here pls
        for i=1:N
            squigglyA{i+1}= squigglyA{i}*allA{i};
            for j=1:i
                squigglyB{i+1,j} = beSquiggly(i,j,allA)*allB{j};%
            end
        end
        toc
        squigglyA = cell2mat(squigglyA);
        Zedd = zeros(size(squigglyB{end}));
        emptyIndex = cellfun('isempty',squigglyB);   % BTW, calling 'isempty' instead of handle @isempty speeds up operation by 40x!!
        squigglyB(emptyIndex) = {Zedd};                    %# Fill empty cells with 0
        squigglyB = cell2mat(squigglyB);
        
end
    
    function [A,B] = getAB(x,p,dt,nDoF)
        % state is [x z]' where z is a 1x1 variable, used to pull
        % linearization to equilibrium from arbitrary points
        
        Zed = zeros(nDoF);
        I = eye(nDoF);
        M = MassMatrixWrapper(x,p);
        MInv = M\eye(size(M));
        
        
%         Hx = -getJacobianStance(q,u); % phiDot seems to be dependent on itself!!! diagonal is not 1, but some other number, wtf?? comes from damping term?
        Hx = StateJacobianWrapper(x,p); %This is only the jacobian for qDotDot
        Hz = JacobianZTermsWrapper(x,p); % jacobian calculation for z-terms (constants with state-augmentation) Calculated separately for convenience in mathematica
        % Cancel out Hz, test: Seems rather accurate, but not quite... just
        % problems with linearization or....?
%         Hz = zeros(size(Hz));
        % Reflect resulting force on foot ??
        qZed = zeros(size(Hz)); % for the q dynamics, z-terms have no weight, so all zeros
        

        % This can probably be implemented more efficiently (a.k.a.
        % cleverly)
        A1 = [I I*dt qZed];
        
        A2 = [dt*MInv*Hx+[Zed I] MInv*Hz*dt ];
        
        A = cat(1,A1,A2);
        A(end+1,end) = 1; % state z dynamics are static: dz/dt = 0, ergo z_K+1 = z_K
        
        
        Bmu = ControlJacobianWrapper(x,p);
%         Bmu = -[0,1,0; 0,0,1]';
        B = [ zeros(size(Bmu));
            dt*MInv*Bmu  ;
            zeros(1,size(Bmu,2)) ];      % last element is z dynamics, always 0. Size of number of control inputs
        
    end

    function Aij = beSquiggly(iE,jE,allA)
        n = size(allA{1},1);
        Aij = eye(n);
        
        if(iE==jE)
            return;
        elseif(iE>jE)
            for it = jE:iE
                %                 tic
                Aij = Aij*allA{it};
                %                 toc
            end
        else
            display('Something odd happening in beSquiggly()');
        end
        
    end